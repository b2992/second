import {NS} from '../../index';
import {getInitScripts} from "lib/libinit";

export async function main(ns: NS) {
  ns.tprint('Initializing..');

  const initFiles = getInitScripts(ns);
  for (const initFile of initFiles) {
    if (ns.isRunning(initFile, 'home')) {
      ns.scriptKill(initFile, 'home');
    }
    ns.spawn(initFile, 1, 'start');
  }
  ns.tprint('Initialized.');
}
