import { install, list, uninstall, update, get } from "lib/libbpm";
export function autocomplete(data, args) {
    if (args.length === 0) {
        return ['install', 'uninstall', 'update', 'list', 'get'];
    }
    else if (args[0] === 'install') {
        return [];
    }
    else if (args[0] === 'uninstall') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else if (args[0] === 'update') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'get') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.args[0] === 'install' && ns.args[1]) {
        await install(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'uninstall' && ns.args[1]) {
        await uninstall(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'update' && ns.args[1]) {
        await update(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'list' && ns.args[1]) {
        await list(ns);
    }
    else if (ns.args[0] === 'get' && ns.args[1]) {
        await get(ns, ns.args[1]);
    }
}
