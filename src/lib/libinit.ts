import {NS} from "../../index";

export function getInitScripts(ns: NS) {
  return ns.ls('home', '/etc/init.d/*');
}
