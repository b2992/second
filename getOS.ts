import {NS} from './index';

export async function main(ns: NS) {
  const baseUrl = 'https://gitlab.com/b2992/second/-/raw/main/';
  const installerFiles = ['files.js', 'installOS.js'];
  if (ns.getHostname() !== "home") {
    throw new Error("Run the script from home");
  }

  let downloadedFiles = [];
  for (let file of installerFiles) {
    if (ns.isRunning(file, 'home')) {
      ns.scriptKill(file, 'home');
    }
    if (await ns.wget(`${baseUrl}${file}`, file)) {
      downloadedFiles.push(file);
    }
  }
  if (installerFiles.length === downloadedFiles.length) {
    ns.spawn('installOS.js', 1);
  } else {
    ns.tprint('ERROR Unable to download all install files!');
  }
}
