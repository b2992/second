import * as gulp from 'gulp';
import * as ts from 'gulp-typescript';
import * as glob from 'glob';
import * as fs from 'fs';

gulp.task('Compile Project', () => {
  const tsProject = ts.createProject('tsconfig.json');
  return gulp.src(['src/**/*.ts'])
    .pipe(tsProject())
    .pipe(gulp.dest('src'));
});
gulp.task('Compile Installer', () => {
  const tsProject = ts.createProject('tsconfig.json');
  return gulp.src(['getOS.ts', 'installOS.ts', 'files.ts'])
    .pipe(tsProject())
    .pipe(gulp.dest('.'));
});
gulp.task('Build File List', async () => {
  let out = [];
  let files = glob.sync('src/**/*.+(script|ns|js|txt)');
  for (let f in files) {
    out.push(files[f].substring(4))
  }
  fs.writeFileSync('files.ts', `export const files = ${JSON.stringify(out)};`);
  return true;
});

// gulp.task('Compile Project', () => {
//   const tsProject = ts.createProject('tsconfig.json');
//   const result = tsProject.src().pipe(tsProject());
//   return result.js.pipe(gulp.dest('dist'));
// });

function defaultTask(cb: any) {
  // place code for your default task here
  cb();
}
gulp.task('default', defaultTask)

// exports.default = defaultTask
